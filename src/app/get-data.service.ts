import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PassDataService } from './pass-data.service';
import { Observable } from 'rxjs';


export interface APIrespose {
  count: number,
  next: string,
  previous: string | null,
  results: Array<object>
}

@Injectable({
  providedIn: 'root'
})

export class GetDataService {

  constructor(
    private readonly http: HttpClient,
    private passedData: PassDataService,
  ) { }

  item: any;
  link:string = "https://swapi.dev/api/"

  getData(tabName: string, n: number): Observable<APIrespose> {
    return this.http.get(`${this.link}${tabName}/?page=${n}`) as Observable<APIrespose>;
  }

  next(n: number, tabName: string): Observable<APIrespose> {
      return this.http.get(`${this.link}${tabName}/?page=${n}`) as Observable<APIrespose>;
  }

  previous(n: number, tabName: string): Observable<APIrespose> {
      return this.http.get(`${this.link}${tabName}/?page=${n}`) as Observable<APIrespose>;
  }
}

