import { Component, OnInit, Input} from '@angular/core';
import { Location } from '@angular/common';
import { PassDataService } from "../pass-data.service";

@Component({
  selector: 'app-list-item-details',
  templateUrl: './list-item-details.component.html',
  styleUrls: ['./list-item-details.component.scss']
})
export class ListItemDetailsComponent implements OnInit {

  constructor(
    private _location: Location,
    private passedData: PassDataService,) {}
  
  item:any;

  ngOnInit(): void {
    this.passedData.currentData.subscribe(item => this.item = item);
    this.filterData()
  }
   
  goBack():void{
    this._location.back();
  }

  filterData(){
    let asArray = Object.entries(this.item);
    const filtered = asArray.filter(([key, value]) => typeof value === 'string');
    this.item =  Object.fromEntries(filtered);
  }
  checkString(str:any){
    return str.startsWith('http');
  }
}
