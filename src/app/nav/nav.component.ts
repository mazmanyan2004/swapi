import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { GetDataService } from '../get-data.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  tabs = ['people', 'planets', 'species', 'starships', 'vehicles'];

  response: any; 

  @Output() getTabNameEvent= new EventEmitter<string>();

  constructor(private readonly httpService: GetDataService) {}

  ngOnInit(): void {}
  
  getTabName(tabName: string):void {
   this.getTabNameEvent.emit(tabName);
  }
}

