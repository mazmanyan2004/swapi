import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PassDataService } from "../pass-data.service";
import { GetDataService } from "../get-data.service";
import { APIrespose } from '../get-data.service';


@Component({
  selector: 'app-list-details',
  templateUrl: './list-details.component.html',
  styleUrls: ['./list-details.component.scss']
})

export class ListDetailsComponent implements OnInit {
  tabName!: string;
  response!: any;
  item: any;
  countArr: Array<number> = [];
  page: number = 1;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private passedData: PassDataService,
    private getDataService: GetDataService,
  ) { }

  ngOnInit(): void {
    this.tabName = (this.activatedRoute.snapshot.routeConfig?.path) as string
    this.getDataService.getData(this.tabName, this.page).subscribe((data: APIrespose) => {
      this.response = data.results;
      this.getButtons(data.count);
    });
    console.log(this.countArr)
  }

  getButtons(count: number) {
    let i = 1;
    while (i <= Math.ceil(count / 10)) {
      this.countArr.push(i);
      i++;
    }
  }

  getElement(item: Object): void {
    this.response = this.passedData.changeData(item);
  }

  getPage(n: number) {
    this.response = undefined;
    this.page = n;
    this.getDataService.getData(this.tabName, n).subscribe((data: APIrespose) => {
      this.response = data.results;
    });
  }

  next() {
    if (this.page < this.countArr.length) {
      this.response = undefined;
      this.getDataService.next(++this.page, this.tabName).subscribe((data: APIrespose) => {
        this.response = data.results;
      });
    }
  }

  previous() {
    if (this.page > 1) {
      this.response = undefined;
      this.getDataService.previous(--this.page, this.tabName).subscribe((data: APIrespose) => {
        this.response = data.results;
      });
    }
  }
}
