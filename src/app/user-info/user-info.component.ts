import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  user: User = {
    name: "Nune",
    surname: "Mazmanyan",
    mail: "mazmanyan2004@gmail.com",
    address: "Erevan - Erebuni"
  }

  constructor() { }

  ngOnInit(): void {
  }
}

