import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListDetailsComponent } from './list-details/list-details.component';
import { ListItemDetailsComponent } from './list-item-details/list-item-details.component';

const routes: Routes = [
  { path: `people`, component: ListDetailsComponent},
  { path: `species`, component: ListDetailsComponent },
  { path: `starships`, component: ListDetailsComponent },
  { path: `vehicles`, component: ListDetailsComponent },
  { path: `planets`, component: ListDetailsComponent },
  
  { path: `people/:i`, component: ListItemDetailsComponent },
  { path: `planets/:i`, component: ListItemDetailsComponent },
  { path: `species/:i`, component: ListItemDetailsComponent },
  { path: `starships/:i`, component: ListItemDetailsComponent },
  { path: `vehicles/:i`, component: ListItemDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
