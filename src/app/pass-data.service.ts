import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PassDataService {
  private dataSource = new BehaviorSubject('');
  currentData = this.dataSource.asObservable();


  constructor() { }
  
  changeData(item:any){
    this.dataSource.next(item);
  }
}
