import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  item!: object;

  constructor(private readonly router: Router){}

  passTabName(tabName: string): void {
    this.router.navigateByUrl(tabName)
  }
}
